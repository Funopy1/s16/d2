let numberA = null
let numberB = null 
let operation = null 

// to retrieve an element from the webpage, we can use querselector
let inputDisplay = document.querySelector('#txt-input-display');
// The document refers to the whole webpage and querySelector is used to select a specific object(HTML elements )
//the queryseelector function takes a string input that is formatted like a CSS selevctor
// This allows us to get a specific element such as CSS selector 
let btnNumbers = document.querySelectorAll('.btn-numbers');


//btnAdd
//btnSubtract
//btnMultiply
//btnDivide 
//btnEqual
//btnDecimal
//btnClearAll
//btnBackspace 

let btnAdd = document.querySelector('#btn-add');
let btnSubtract = document.querySelector('#btn-subtract');
let btnMultiply = document.querySelector ('#btn-multiply');
let btnDivide = document.querySelector ('#btn-divide');
let btnEqual = document.querySelector ('#btn-equal');
let btnDecimal = document.querySelector ('#btn-decimal');


let btnClearAll = document.querySelector ('#btn-clear-all');
let btnBackspace = document.querySelector ('#btn-backspace');


// para mailagay sa display 
btnNumbers.forEach(function(btnNumber){
	btnNumber.onclick = () => {
		inputDisplay.value += btnNumber.textContent;
	}
})

btnAdd.onclick = () => {   //cinall
	if (numberA == null) {
		numberA = Number(inputDisplay.value); // para lumabas yung value na ininput
		operation = 'addition';
		inputDisplay.value = null; 
	}
	else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA + numberB;
		operation = 'addition';
		numberB.value = null ; 
		inputDisplay.value = null ; 
	}
}

btnSubtract.onclick = () => {
	if (numberA == null){
		numberA = Number(inputDisplay.value);
		operation = 'subtraction'
		inputDisplay.value = null; 
	}
	else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA - numberB;
		operation = 'subtraction';
		numberB.value = null ; 
		inputDisplay.value = null ; 
	}
}

btnMultiply.onclick = () => {
	if (numberA == null){
		numberA = Number(inputDisplay.value);
		operation = 'multiply'
		inputDisplay.value = null; 
	}
	else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA * numberB;
		operation = 'multiply';
		numberB.value = null ; 
		inputDisplay.value = null ; 
	}
}

btnDivide.onclick = () => {
	if (numberA == null){
		numberA = Number(inputDisplay.value);
		operation = 'divide'
		inputDisplay.value = null; 
	}
	else if (numberB == null){
		numberB = Number(inputDisplay.value);
		numberA = numberA * numberB;
		operation = 'divide';
		numberB.value = null ; 
		inputDisplay.value = null ; 
	}
}


btnClearAll.onclick = () => {
	numberA = null;
	numberB = null;
	operation = null; 
	inputDisplay.value = null;
}

btnBackspace.onclick = () => {
	inputDisplay.value = inputDisplay.value.slice(0,-1); // para mabawasan 
}

btnDecimal.onclick = () => {
	if (!inputDisplay.value.includes('.')){
		inputDisplay.value = inputDisplay.value + btnDecimal.textContent;
	}
}

btnEqual.onclick = () => {
	if (numberB == null && inputDisplay.value !==''){
		numberB = inputDisplay.value; 
	}
	if (operation == 'addition'){
		inputDisplay.value = Number(numberA) + Number(numberB);
	}
	if (operation == 'subtraction'){
		inputDisplay.value = Number(numberA) - Number(numberB);
	}
	if (operation == 'multiply'){
		inputDisplay.value = Number(numberA) * Number(numberB);
	}
	if (operation == 'divide'){
		inputDisplay.value = Number(numberA) / Number(numberB);
	}
}


// Name 

const addName = () =>{
	if (firstName.value !== ""){
	fullName.innerText = `${firstName.value}`;
	}

	if ( lastName.value !== ""){
	fullName.innerText = `${firstName.value} ${lastName.value}`;
	}

	};

	firstName.addEventListener('input',addName);
	lastName.addEventListener('input',addName);